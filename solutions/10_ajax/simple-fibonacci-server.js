// Prerequisites: 
//     NodeJS and NPM installed
// Preparation:
//     npm init
//     npm install --save express
//     npm install --save body-parser
// Execution:
//     node simple-fibonacci-server.js

let express    = require('express');
let bodyParser = require('body-parser');
let app        = express();
let router     = express.Router();
let port       = 8000;

function fib(n) {
    return (n <= 2) ? 1 : fib(n - 2) + fib(n - 1);
}
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", 
        "*");
    res.header("Access-Control-Allow-Headers", 
        "Origin, X-Requested-With, Content-Type, Accept, Cache-Control");
    next();
});
router.route('/')
    .get((req, res) => {
        res.json({msg: 'simple fibonacci server'});
    })
    .post((req, res) => {
        console.log('REQ:', req.body);
        let arg    = req.body.argument;
        let result = fib(arg);
        console.log('RPLY:', result);
        res.json({
            success: 1,
            argument: arg,
            result: result
        });
    })
;
app.use('/fib', router);
app.listen(port);
console.log(`started http://localhost:${port}/fib`);


