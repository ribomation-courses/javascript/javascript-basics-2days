// Prerequisites: 
//     NodeJS and NPM installed
// Preparation:
//     npm init
//     npm install --save express
// Execution:
//     node simple-rest-server.js

let express = require('express');
let app     = express();
let fs      = require("fs");

app.get('/fetch', (req, res) => {
    fs.readFile(__dirname + "/" + "King-Richard-III-snippet.txt", 'utf8',
        function (err, data) {
            console.log(data);
            res.end(data);
        });
});

let server = app.listen(8081, () => {
    let host = server.address().address;
    let port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port)
});

