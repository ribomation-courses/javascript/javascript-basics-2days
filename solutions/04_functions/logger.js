let logger = (function () {
    let indent = 0, lineno = 0;

    function tab(n) {
        let t = '';
        while (n-- > 0) t += '  ';
        return t;
    }
    
    function log(msg) {
        console.log((++lineno) + ': ' + tab(indent) + msg);
    }

    return {
        enter: function (msg) {
            log(msg);
            ++indent;
        },
        print: function (msg) {
            log(msg);
        },
        leave: function (msg) {
            --indent;
            log(msg);
        }
    };
}());

function fib(n) {
    logger.enter('FIB(' + n + ')');
    let result = (n <= 2) ? 1 : fib(n - 2) + fib(n - 1);
    logger.leave('FIB(' + n + ') --> ' + result);
    return result;
}

let N = 10;
logger.print('fib(' + N + ') = ' + fib(N));
