let Numbers = (function () {
    function sum(n) {
        return n <= 0 ? 0 : n + sum(n - 1);  // n*(+1)/2
    }

    function prod(n) {
        return n <= 1 ? 1 : n * prod(n - 1);
    }

    function fib(n) {
        return n <= 2 ? 1 : fib(n - 2) + fib(n - 1);
    }

    function tbl(n) {
        if (n < 1) return;

        console.log('k\tSUM\tFIB\tPROD');
        for (let k = 1; k <= n; ++k) {
            let s = sum(k);
            let p = prod(k);
            let f = fib(k);
            console.log(`${k}\t${s}\t${f}\t${p}`);
        }
    }

    return {
        sum: sum, 
        prod: prod, 
        fib: fib, 
        tbl: tbl
    };
}());

Numbers.tbl(process.argv[2] || 10);
