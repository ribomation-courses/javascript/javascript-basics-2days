let T = ((function () {
    'use strict';

    function toFahrenheit(c) {
        return (9 / 5) * c + 32;
    }

    return {
        convert: function (lb, ub) {
            if (ub <= lb) {
                throw 'invalid range: [' + lb + ', ' + ub + ']';
            }

            let result = [];  
            for (let c = lb; c <= ub; ++c) {
                result.push(toFahrenheit(c));
            }

            return result;
        }
    };
})());

let lower = 10, 
    upper = 25, 
    fahrenheit = T.convert(lower, upper);
console.log('Range [' + lower + ', ' + upper + '] C --> F:\n' + fahrenheit);
