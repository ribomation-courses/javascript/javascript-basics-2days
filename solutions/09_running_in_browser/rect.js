function Rect(opts) {
    const colors = ['#F00', '#0F0', '#00F', '#FFA500'];
    function uniform(lb, ub) {
        return Math.round(lb + Math.random() * (ub - lb));
    }

    this.id     = opts.id;
    this.side   = opts.side;
    this.width  = opts.width;
    this.height = opts.height;
    this.x      = uniform(0, opts.width - opts.side);
    this.y      = uniform(0, opts.height - opts.side);
    this.dx     = uniform(1, 3);
    this.dy     = uniform(1, 3);

    this.widget                       = document.createElement('div');
    this.widget.id                    = 'rect-' + (opts.id + 1);
    this.widget.style.position        = 'absolute';
    this.widget.style.left            = this.x + 'px';
    this.widget.style.top             = this.y + 'px';
    this.widget.style.width           = this.side + 'px';
    this.widget.style.height          = this.side + 'px';
    this.widget.style.backgroundColor = colors[this.id % colors.length];
    
    opts.parent.appendChild(this.widget);
}

Rect.prototype.move = function () {
    this.widget.style.left = this.x + 'px';
    this.widget.style.top  = this.y + 'px';
    this.x += this.dx;
    this.y += this.dy;

    function outside(lb, v, ub) {
        return !((lb <= v) && (v <= ub));
    }

    if (outside(0, this.x, this.width - this.side)) this.dx = -this.dx;
    if (outside(0, this.y, this.height - this.side)) this.dy = -this.dy;
};
