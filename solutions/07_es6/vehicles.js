function PRINT(msg) {
    if (msg.toString) {
        msg = msg.toString();
    }
    if (this.console) {
        console.log(msg);
    } else {
        print(msg);
    }
}

class Vehicle {
    constructor(licno) {
        this._licno = licno || 'ABC123';
    }

    get licno() {
        return this._licno
    }

    toString() {
        return `${this.licno}`;
    }

    move() {
        PRINT('*** Vehicle moving');
    }
}

class Car extends Vehicle {
    constructor(licno, turbo) {
        super(licno);
        this._turbo = turbo;
    }

    get turbo() {
        return this._turbo;
    }

    set turbo(t) {
        this._turbo = t;
        return this._turbo;
    }

    toString() {
        return `Car{${super.toString()}, turbo=${this.turbo}}`;
    }

    move() {
        PRINT(`${this.toString()} is driving`);
    }
}

class Boat extends Vehicle {
    constructor(name, hasMotor) {
        super(name);
        this._motor = hasMotor;
    }
    
    get motor() {return this._motor;}
    
    toString() {
        return `Boat{${super.toString()}, motor=${this.motor}}`;
    }

    move() {
        PRINT(`${this.toString()} is sailing`);
    }
}

class Plane extends Vehicle {
    constructor(id, hasPropeller) {
        super(id);
        this._propeller = hasPropeller;
    }
    
    get propeller() {return this._propeller;}
    
    toString() {
        return `Plane{${super.toString()}, propeller=${this.propeller}}`;
    }

    move() {
        PRINT(`${this.toString()} is flying`);
    }
}


let vehicles = [
    new Car('QWE123', true),
    new Boat('S/S Amanda', false),
    new Plane('Red Baron', true)
];

vehicles.forEach((v) => v.move());
