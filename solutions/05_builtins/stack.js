
function mkStack(N) {
    let stk = [];
    for (let k = 1; k <= N; ++k) {
        stk.push(k);
    }
    return stk;
}

function drainStack(stk) {
    while (stk.length) {
        console.log(stk.pop());
    }
}

let N = process.argv.length > 2 ? +process.argv[2] : 16;
drainStack( mkStack(N) );
