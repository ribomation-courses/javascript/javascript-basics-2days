// RUN: node word-freq.js [filename]
const fs   = require('fs');
const path = require('path');

function mostFrequentWords(text, opts) {
    opts = opts || {};
    const minWordSize   = opts.minWordSize || 4;
    const maxWordsCount = opts.maxWordsCount || 15;
    let data = text
        .split(/[^a-z’]+/i)
        .filter(function (w) {
            return w.length >= minWordSize;
        })
        .map(function (w) {
            return w.toLowerCase();
        })
        .reduce(function (freq, word) {
            freq[word] = 1 + (+freq[word] || 0);
            return freq;
        }, {})
        ;
    let pairs = [];
    for (let w in data) pairs.push([w, data[w]]);
    pairs.sort(function (left, right) {
        return right[1] - left[1];
    });
    return pairs.slice(0, maxWordsCount)
}

function parseArgs(args) {
    let opts = {
        filename: './The-Three-Musketeers.txt',
        maxWordsCount: 25,
        minWordSize: 4
    };

    for (let k = 2; k < args.length; ++k) {
        let arg = args[k];
        switch (arg) {
            case '-f':
                opts.filename = args[++k];
                break;
            case '-w':
                opts.maxWordsCount = parseInt(args[++k]);
                break;
            case '-s':
                opts.minWordSize = parseInt(args[++k]);
                break;
            default:
                console.warn('unknown:', arg);
                console.warn('usage:', 'node', path.basename(process.argv[1]), '[-f <file>]', '[-w <max words>]', '[-s <min size>]');
                process.exit(1);
        }
    }

    return opts;
}


// --- main prog ---
let opts   = parseArgs(process.argv);
let text   = fs.readFileSync(opts.filename, { encoding: 'utf-8' });
let result = mostFrequentWords(text, opts);
result.forEach(function (f) {
    let word = f[0];
    let freq = f[1];
    console.log('%s: %i', word, freq);
});
