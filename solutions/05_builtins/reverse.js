function reverse(txt) {
    return txt.split(/\s+/).reverse().join(' ');
}

function demo(txt) {
    console.log(txt + ' --> ' + reverse(txt));
}

if (process.argv.length > 2) {
    for (let k = 2; k < process.argv.length; ++k) {
        demo(process.argv[k]);
    }
} else {
    demo('abc def \t ghi');
    demo("Foobar strikes again");
}
