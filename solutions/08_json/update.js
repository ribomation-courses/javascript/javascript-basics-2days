/********
    Given the ./package.json you can download and
    install the dependency yamljs and its transitive
    dependencies using the command:
        npm install
    Run NodeJS application as:
        node update.js [transaction yml file]
*/

let YAML         = require('yamljs');
let filename     = process.argv[2] || './transactions.yml';
let transactions = YAML.load(filename);

transactions.forEach(function (t) {
    t.amount *= 100;
});

let replacer = (key, value) => {
    //console.log(`*** key=${key}, value=${value} value.type=${typeof value}`);
    if (key === 'date') {
        return value.substr(0, value.indexOf('T'));
    }
    return value;
};

console.log(JSON.stringify(transactions, replacer, 4));
