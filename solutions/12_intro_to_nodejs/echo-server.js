var http = require('http');
var port = 9000;

http.createServer(handler)
    .listen(port);

console.log(`Listening on port ${port}`);

function handler(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    req.on('data', function (payload) {
        console.log(`recv: ${payload}`);
        res.write(`echo:  ${payload.toString().toUpperCase()}`);
    });
    req.on('end', function () {
        res.end();
    });
}


