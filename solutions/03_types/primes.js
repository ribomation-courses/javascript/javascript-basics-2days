'use strict';

let N       = 100;
let numbers = [];
for (let k = 2; k <= N; ++k) {
    numbers[k] = true;
}
for (let p = 2; p < numbers.length; ++p) {
    while (!numbers[p] && p < numbers.length) {
        ++p;
    }
    for (let k = 2 * p; k < numbers.length; k += p) {
        numbers[k] = false;
    }
}

let primes = [];
for (let p = 2; p < numbers.length; ++p) {
    if (numbers[p]) primes.push(p);
}

console.log(N + ': ' + primes);
