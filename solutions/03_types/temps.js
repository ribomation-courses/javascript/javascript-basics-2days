'use strict';

let N         = 100;
let celsius   = [];
let farenheit = [];
for (let k = 0; k <= N; ++k) {
    celsius[k]   = k;
    farenheit[k] = (9 / 5) * k + 32;
}

console.log('C', '-->', 'F');
for (let k in celsius) {
    console.log(celsius[k], '-->', farenheit[k]);
}
