JavaScript Basics, 2 days
====

Welcome to this course.

Here you will find
* Installation instructions
* Solutions to the programming exercises

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/javascript/javascript-basics-2days
    cd javascript-basics-2days

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have
the following programs installed:

* NodeJS
  https://nodejs.org/en/download/
* Google Chrome
  https://www.google.se/chrome/browser/desktop/
* JetBrains WebStorm
  https://www.jetbrains.com/webstorm/download

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

